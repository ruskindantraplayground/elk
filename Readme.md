## Tutorial
https://markheath.net/post/exploring-elasticsearch-with-docker

## Notes
1. Running docker-compose-v1.yml  
`docker-compose -f docker-compose-v1.yml up -d`
1. Check cluster health  
`curl http://localhost:9200/_cluster/health?pretty`
1. Create an index  
`curl -X PUT "localhost:9200/customer?pretty"`
1. Add a new document  
`curl -X PUT "localhost:9200/customer/_doc/1?pretty" -H 'Content-Type: application/json' -d'{"name": "Mark Heath" }'`
1. View documents in this index  
`curl localhost:9200/customer/_search\?pretty`

## Upgrading to Elastic 6.4.2
1. We will use `docker-compose-v2.yml`
1. Note that the containers will be recreated but the data will remain intact
1. Use the command:  
`docker-compose -f docker-compose-v2.yml up -d`
1. The containers will be _recreated_
1. Note the new container IDs
1. Check that the index is still present  
`curl localhost:9200/customer/_search\?pretty`
    1. Note that sometimes you have to escapse `?` using `\?`
1. Add another document  
`curl -X PUT "localhost:9200/customer/_doc/2?pretty" -H 'Content-Type: application/json' -d'{"name": "Steph Heath"}'`

## Upgrading to a 3-node cluster
1. `docker-compose -f docker-compose-v3.yml up -d`
1. Note the new setting `discovery.zen.minimum_master_nodes=2` to avoid [split brain issue](https://www.elastic.co/guide/en/elasticsearch/reference/2.3/modules-node.html#split-brain)
1. Check that the index is still present  
`curl localhost:9200/customer/_search?pretty`

## Introducing Head and Kibana
1. Head: UI to interact with Elastic ([ref](https://blog.ruanbekker.com/blog/2018/04/29/running-a-3-node-elasticsearch-cluster-with-docker-compose-on-your-laptop-for-testing/))
1. `docker-compose -f docker-compose-v4.yml up -d`
1. Navigate to the HEAD ui via `http://localhost:9100`

## Teardown
1. Without removing volumes: `docker-compose -f docker-compose-v4.yml down`
1. Removing volumes: `docker-compose -f docker-compose-v4.yml down -v`